﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Lab3Wpf
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //создаем регулярное выражение, описывающее правило ввода
        //в данном случае, это символы от 1 до 6
        Regex inputRegex = new Regex(@"^[1-6]$");

        int criteria, alternative;

        Double[][] u, z;
        DataTable dtAlternativeToCriteria;
        DataTable dtLenght;
        DataTable dtW;
        DataTable dtAgree;
        DataTable dtDisagree;

        public MainWindow()
        {
            InitializeComponent();

            InitializeEvent();
        }

        private void InitializeEvent()
        {
            textCriteria.PreviewTextInput += Text_PreviewTextInput;
            textAlternative.PreviewTextInput += Text_PreviewTextInput;

            btnPrepare.Click += BtnPrepare_Click;
            btnExample.Click += BtnExample_Click;
            btnMatch.Click += BtnMatch_Click;
            btnBorder.Click += BtnBorder_Click;
        }
        
        private void BtnBorder_Click(object sender, RoutedEventArgs e)
        {
            textLog.Text = "";

            int end = 0, step = 1;
            double[][] u_temp = u, z_temp = z;
            double u_max = Double.Parse(textU.Text);
            double z_min = Double.Parse(textZ.Text);

            int[] priority = new int[u_temp.Count()];

            do
            {
                textLog.Text += "--------- Шаг " + step + " ---------\n"; 
                textLog.Text += "U ≥ " + u_max + "\n";
                textLog.Text += "Z ≤ " + z_min + "\n";

                /// Расчет приоритетов
                for (int i = 0; i < priority.Count(); i++)
                    for (int j = 0; j < priority.Count(); j++)
                        if (i == j || priority[i] == -1)
                            continue;
                        else if (u[i][j] >= u_max && z[i][j] <= z_min)
                            priority[i]++;

                int min = GetMinPlus(priority);

                textLog.Text += "Доминируемые альтернативы: \n";
                for (int i = 0; i < priority.Count(); i++)
                    if (priority[i] > min)
                        textLog.Text += "A" + (i + 1) + "; ";
                textLog.Text += "\n";

                textLog.Text += step + " ядро альтернатив: \n";
                for (int i = 0; i < priority.Count(); i++)
                    if (priority[i] == min)
                        textLog.Text += "A" + (i + 1) + "; ";
                textLog.Text += "\n\n";

                for (int i = 0; i < priority.Count(); i++)
                    if (priority[i] == min)
                        priority[i] = -1;
                    else if (priority[i] != -1)
                        priority[i] = 0;

                GetBorder(out u_max, out z_min, priority);

                step++;

                for (int i = 0; i < priority.Count(); i++)
                    if (priority[i] == -1)
                        end++;
            } while (end + 1 < priority.Count());

            textLog.Text += "--------- Шаг " + step + " ---------\n";
            textLog.Text += step + " ядро альтернатив: \n";
            for (int i = 0; i < priority.Count(); i++)
                if (priority[i] >= 0)
                    textLog.Text += "A" + (i + 1) + "; ";
            textLog.Text += "\n\n";
        }

        private int GetMinPlus(int[] a)
        {
            int min = -1;

            for (int i = 0; i < a.Count(); i++)
            {
                if (min == -1 && a[i] >= 0)
                    min = a[i];
                else if (min > a[i] && a[i] >= 0)
                    min = a[i];
            }

            return min;
        }

        private void GetBorder(out double u_max, out double z_min, int[] priority)
        {
            u_max = 0;
            z_min = 1;

            for (int i = 0; i < u.Count(); i++)
            {
                for (int j = 0; j < u[i].Count(); j++)
                {
                    if (i == j || priority[i] == -1 || priority[j] == -1)
                        continue;

                    if (u[i][j] > u_max)
                    {
                        u_max = u[i][j];
                        z_min = z[i][j];
                    }
                    else if (u[i][j] == u_max && z[i][j] < z_min)
                    {
                        z_min = z[i][j];
                    }
                }
            }
        }

        private void BtnMatch_Click(object sender, RoutedEventArgs e)
        {
            textLog.Text = "";
            u = new double[alternative][];
            z = new double[alternative][];
            for (int i = 0; i < alternative; i++)
            {
                u[i] = new double[alternative];
                z[i] = new double[alternative];
            }

            int w_sum = 0;
            for (int i = 0; i < criteria; i++)
            {
                w_sum += Int32.Parse(dtW.Rows[0][i].ToString());
            }
            
            /// Матрица согласия
            for (int i = 0; i < alternative; i++)
            {
                for (int j = 0; j < alternative; j++)
                {
                    if (i == j)
                        continue;

                    Dictionary<char, string> temp = GetI(i, j);
                    string add;
                    string normal;
                    int sum = 0;

                    if (temp.TryGetValue('+', out add) && temp.TryGetValue('=', out normal))
                    {
                        foreach (string s in add.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            sum += Int32.Parse(dtW.Rows[0][Int32.Parse(s)].ToString());
                        }
                        foreach (string s in normal.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            sum += Int32.Parse(dtW.Rows[0][Int32.Parse(s)].ToString());
                        }
                    }

                    dtAgree.Columns[j + 1].ReadOnly = false;
                    dtAgree.Rows[i][j + 1] = ((double)sum / w_sum).ToString("N2");
                    dtAgree.Columns[j + 1].ReadOnly = true;
                    u[i][j] = Double.Parse(dtAgree.Rows[i][j + 1].ToString());
                   
                }
            }
            
            /// Матрица несогласия
            for (int i = 0; i < alternative; i++)
            {
                for (int j = 0; j < alternative; j++)
                {
                    if (i == j)
                        continue;

                    Dictionary<char, string> temp = GetI(i, j);
                    string sub;

                    List<double> list = new List<double>();

                    if (temp.TryGetValue('-', out sub))
                    {
                        foreach (string s in sub.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            list.Add(
                            Math.Abs(
                                Double.Parse(dtAlternativeToCriteria.Rows[Int32.Parse(s)][i + 1].ToString())
                                -
                                Double.Parse(dtAlternativeToCriteria.Rows[Int32.Parse(s)][j + 1].ToString())
                            )
                            /
                            Double.Parse(dtLenght.Rows[0][Int32.Parse(s)].ToString())
                        );
                        }
                    }
                    dtDisagree.Columns[j + 1].ReadOnly = false;
                    if (list.Count == 0)
                    {
                        dtDisagree.Rows[i][j + 1] = 0;
                        z[i][j] = 0;
                    }
                    else
                    {
                        dtDisagree.Rows[i][j + 1] = list.Max().ToString("N2");
                        z[i][j] = Double.Parse(dtDisagree.Rows[i][j + 1].ToString());
                    }
                    dtDisagree.Columns[j + 1].ReadOnly = true;

                }
            }

            GetBorder();
        }
    
        private void GetBorder()
        {
            double max = 0, min = 1;

            for (int i = 0; i < u.Count(); i++)
            {
                for (int j = 0; j< u[i].Count(); j++)
                {
                    if (i == j)
                        continue;

                    if (u[i][j] > max)
                    {
                        max = u[i][j];
                        min = z[i][j];
                    } else if (u[i][j] == max && z[i][j] < min)
                    {
                        min = z[i][j];
                    }
                }
            }

            textU.Text = max.ToString();
            textZ.Text = min.ToString();
        }

        private Dictionary<char, string> GetI(int a1, int a2)
        {
            Dictionary<char, string> result = new Dictionary<char, string>();

            string add = "";
            string sub = "";
            string normal = "";

            for (int i = 0; i < criteria; i++)
            {
                if (Double.Parse(dtAlternativeToCriteria.Rows[i][a1+1].ToString()) < Double.Parse(dtAlternativeToCriteria.Rows[i][a2 + 1].ToString()))
                {
                    if (add.Length != 0)
                    {
                        add += ",";
                    }
                    add += i;
                }
                else if (Double.Parse(dtAlternativeToCriteria.Rows[i][a1 + 1].ToString()) == Double.Parse(dtAlternativeToCriteria.Rows[i][a2 + 1].ToString()))
                {
                    if (normal.Length != 0)
                    {
                        normal += ",";
                    }
                    normal += i;
                }
                else
                {
                    if (sub.Length != 0)
                    {
                        sub += ",";
                    }
                    sub += i;
                }
            }

            result.Add('+', add);
            result.Add('-', sub);
            result.Add('=', normal);

            return result;
        } 

        private void BtnExample_Click(object sender, RoutedEventArgs e)
        {
            textCriteria.Text = "3";
            textAlternative.Text = "4";

            BtnPrepare_Click(null, null);

            string[,] matrixAlternativeToCriteria = {
                {"180", "170", "160", "150"},
                {"70",  "40",  "55",  "50" },
                {"10",  "15",  "20",  "25" }
            };

            for (int i = 0; i < criteria; i++)
            {
                for (int j = 0; j < alternative; j++)
                {
                    dtAlternativeToCriteria.Rows[i][j + 1] = matrixAlternativeToCriteria[i, j];
                }
            }

            string[] massiveLenght =
            {
                "100", "50", "45"
            };

            for (int i = 0; i < criteria; i++)
            {
                dtLenght.Rows[0][i] = massiveLenght[i];
            }

            string[] massiveW=
            {
                "3", "2", "1"
            };

            for (int i = 0; i < criteria; i++)
            {
                dtW.Rows[0][i] = massiveW[i];
            }


            BtnMatch_Click(null, null);
        }

        private void BtnPrepare_Click(object sender, RoutedEventArgs e)
        {
            if (Int32.TryParse(textCriteria.Text, out criteria) && Int32.TryParse(textAlternative.Text, out alternative))
            {
                PrepareAlternativeToCriteria();
                PrepareLenght();
                PrepareW();
                PrepareAgree();
                PrepareDisagree();
            }
            else
            {
                MessageBox.Show("Неправильно задано количество критериев и альтернатив!", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        
        private void PrepareAlternativeToCriteria()
        {
            dtAlternativeToCriteria = new DataTable();

            dtAlternativeToCriteria.Columns.Clear();
            dtAlternativeToCriteria.Columns.Add("#");
            for (int i = 0; i < alternative; i++)
            {
                dtAlternativeToCriteria.Columns.Add("Alt #" + (i + 1));
            }

            dtAlternativeToCriteria.Rows.Clear();
            for (int i = 0; i < criteria; i++)
            {
                dtAlternativeToCriteria.Rows.Add("Cr #" + (i + 1));
            }

            dtAlternativeToCriteria.Columns[0].ReadOnly = true;

            dataAlternativeToCriteria.DataContext = dtAlternativeToCriteria.DefaultView;
        }
        private void PrepareLenght()
        {
            dtLenght = new DataTable();

            dtLenght.Columns.Clear();
            dtLenght.Rows.Clear();
            for (int i = 0; i < criteria; i++)
            {
                dtLenght.Columns.Add("L #" + (i + 1));
            }

            dtLenght.Rows.Add();

            dataLenght.DataContext = dtLenght.DefaultView;
        }
        private void PrepareW()
        {
            dtW = new DataTable();

            dtW.Columns.Clear();
            dtW.Rows.Clear();
            for (int i = 0; i < criteria; i++)
            {
                dtW.Columns.Add("w" + (i + 1));
            }

            dtW.Rows.Add();

            dataW.DataContext = dtW.DefaultView;
        }
        private void PrepareAgree()
        {
            dtAgree = new DataTable();
            dtAgree.Rows.Clear();
            dtAgree.Columns.Clear();
            dtAgree.Columns.Add("Z");
            dtAgree.Columns[0].ReadOnly = true;
            for (int i = 0; i < alternative; i++)
            {
                dtAgree.Rows.Add("#" + (i + 1));
                dtAgree.Columns.Add("#" + (i + 1));
                dtAgree.Rows[i][i + 1] = "*";
                dtAgree.Columns[i + 1].ReadOnly = true;
            }

            dataAgree.DataContext = dtAgree.DefaultView;
        }

        private void textLog_Scroll(object sender, ScrollEventArgs e)
        {

        }

        private void PrepareDisagree()
        {
            dtDisagree = new DataTable();
            dtDisagree.Rows.Clear();
            dtDisagree.Columns.Clear();
            dtDisagree.Columns.Add("U");
            for (int i = 0; i < alternative; i++)
            {
                dtDisagree.Rows.Add("#" + (i + 1));
                dtDisagree.Columns.Add("#" + (i + 1));
                dtDisagree.Rows[i][i + 1] = "*";
                dtDisagree.Columns[i + 1].ReadOnly = true;
            }
            dtDisagree.Columns[0].ReadOnly = true;


            dataDisagree.DataContext = dtDisagree.DefaultView;
        }

        private void Text_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            //проверяем или подходит введенный символ нашему правилу
            Match match = inputRegex.Match(e.Text);
            //и проверяем или выполняется условие
            //если количество символов в строке больше или равно одному либо
            //если введенный символ не подходит нашему правилу
            if ((sender as TextBox).Text.Length >= 1 || !match.Success)
            {
                //то обработка события прекращается и ввода неправильного символа не происходит
                e.Handled = true;
            }
        }
    }
}